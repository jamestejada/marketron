# CapRadio Marketron Renderer
This program takes .rtf files exported from Marketron (with scripts included) and renders them as Google Sheets in a specific destination folder.

## Setup
### Environment
- Ubuntu Server 20.04 or Ubuntu 20.04 WSL (Windows Subsystem Linux)
- Python 3.8

1. ### Dependencies
    - Google API Credentials via service account
    - Slack Workflow Webhook (optional)
    - Access to Paperless Logs Network Drive (contains Marketron rtf output)

1. ### Environment Variables (`./marketron/credentials/.env`)
    - `MARKETRON_FOLDER_ID` - the ID string for the Google Drive folder in which your Google Sheets will be placed.
    - `{stationCallLetters}_FOLDER_ID` - The ID string for the Google Drive folders for each station within the Marketron Google Drive folder. `{stationCallLetters}` are the call letters for the station whose marketron log you have produced (e.g. 'KWOD', 'WZZO').
    - `SLACK_WEBHOOK` - The full url to your slack webhook
        -  JSON payload
            ```json
            {
                "list_of_links": {names_of_logs_with_links_separated_by_new_line_characters}
            }
            ```

1. ### Set up [Slack Webhook](./documentation/slack-webhook.md) (optional)
    This webhook will notify you when new logs are processed and send the direct links via Slack

1. ### Set up a [Google Service Account](https://cloud.google.com/docs/authentication/production#create_service_account)
    - Download the JSON keyfile to `./marketron/credentials/` and rename the file `credentials.json`

1. ### [Mount Paperless Network Drive](./documentation/mounting-drives.md)
    This is necessary for the program to be able to access the paperless log .rtf files on the shared drive

1. ### Create a Virtual environment
    Within the root project directory, execute the virtual environment script. This will create a virtual environment and enter it.
    ```
    $ . environ
    ```

1. ### Install Python Dependencies
    Install dependencies from the `requirements.txt` file using the python package manager
    ```
    (venv) $ pip install -r requirements.txt
    ```

1. ### Run Tests
    ```
    $ . tron tests
    ```

1. ### Set up Cron job
    a. Edit the crontab
    ```
    $ sudo nano /etc/crontab
    ```
    b. Set up a cron job (or multiple, depending on when you want the cron job to run)
    ```bash
    # /etc/crontab

    * 39 * * * root cd /home/{username}/marketron && bash tron
    24 19 * * 5 root cd /home/{username}/marketron && bash tron archive
    20 3 * * 6 root cd /home/{username}/marketron && bash tron reset
    ```
    NOTE: you can use [crontab.guru](https://crontab.guru/) to verify your cron schedule expressions. This is important because the cron service will not execute anything if there is an error in the crontab file. 

    c. Restart cron service
    ```
    $ sudo service cron restart
    ```
    and to check if everything is ok
    ```
    $ sudo service cron status
    ```
## Using this Program

### Execution Flags

The program can follow certain execution paths by passing in the string listed in the [ ].

- RESET [`reset`, `clear`, `clean`] - empties trash in Google Drive and clears the `data` directory of documents. 
- TESTS [`tests`, `test`] - runs tests suite
- COPY [`copy`, `get`] - copies files from the paperless log network share
- ONE [`one`] - empties Google Drive trash, copies all new paperless logs, and converts, parses and renders a spreadsheet for only one log. This is for testing only. 
- ARCHIVE [`archive`] - takes from files from the Marketron archive folder on Google Drive and saves them as a pdf on the paperless logs network share.

### Running this program manually

As shown above in the `crontab` file, the entry point for this program is the bash script `tron` at the top level directory for this program. Execution flags can be passed to the `tron` script as parameters and it will be passed as parameters to the Python module. 

Examples: 
```
$ . tron 
```

```
$ . tron tests
```

```
$ . tron archive
```

## Modules



## The Future of this Project
### Room for improvement
1. #### Write less files
   - Right now the a text file is written after the stripping rtf formatting, and a json file is written after structuring the text file into JSON. These processes could be done completely in memory to save resources. 

1. #### Cleanup of the `./marketron/write.py` modules
    - The Google Sheets API takes a massive JSON payload to make changes to the spreadsheet. I believe there is some duplication of code that renders these JSON objects that could be done more efficiently using methods. 

1. #### The parsing of the text file could be made more clear
    - I think the code that parses the text file is a little bit confusing and could be cleaned up
    - The `Data_Extractor.convert_to_dict()` method is confusing to reason about and probably could be made a lot more clear. Sorry about that. 

### Possibly automating marketron reconciliation

#### Background
At a commercial radio station, because they commercials play from files, the extract an 'asplay' log that Marketron can read, that has a record of what played and at what time. Then reconciliation (the process of figuring out if all the underwriting contracts have been satisfied) is done within Marketron using this file. 

In public radio, many stations have local hosts that will read underwriting live. This poses a challenge for reconciliation becuase then reconciliation must be done manually.

#### Leveraging built in Marketron features for Automated reconciliation
By using Marketron's built in systems of automating reconciliation, we can possibly automate reconciliation of the local host underwriting reads. We have moved one more step in this direction by create a digital log for local hosts to use. In the future if we wanted to possibly reconcile automatically, using this system we would need to add the following steps for this program:
1. After a day's log is finished, get information from the digital log about which underwriting was read and which wasn't. 
2. Generate an 'asplay' log that would tell Marketron about which 'spots' did not play. 

This can be done using built in protocols. After talking with [Roy Harrison](mailto:rharrison@MARKETRON.COM) at Marketron he suggested a few protocols used by playlist automation vendors. One of these is [Wireready](http://wireready.com/). The reason the Wireready protocol might be easier for this program to use, is that Wireready uses a simple text file to do reconciliation with Marketron. If we can mimic the Wireready asplay text file, we can allow Marketron to perform automatic reconciliation with this file. 

#### The Wireready Protocol
This is what I have received from Roy regarding the wireready protocol:
> Generic Audit File: 
>             
> 
> General file details:
> File name: iiyymmdd.201, where:
> ii = station identifier, "00"-"99" but commonly "01" or  > "02". Any two digit identifier  will 
> suffice if it is unique to the station.
>                        yy = Last 2 digits of the year. Examples: 96=1996, 02=2002
>                        mm = Month number, always 2 digits, 01-12
>                        dd = Day of month, always 2 digits, 01-31
>                 The file name is always 8 characters followed by the ".201" > extension.
>                 
>             
> ** The file is organized into "lines" of printable ASCII. Each line is  > one record. Lines are fixed
> lengths (number of bytes). Each line is terminated by a CR LF (OD OA hexadecimal (HEX> ())), which is not      
> specifically shown below.
> ** This file is to report spots that aired.  
> ** The file records one day's activity.  The midnight boundary must > not  be crossed (i.e., maximum
> range of the file is 00:00-23:59).
> ** The delimiter character of a HEX(7C) (the "pipe" symbol) is used  > between fields on a line.
> ** The numbers shown below for each field are the starting byte > position  in the record followed by
> the length of the field. Example:
> X,Y  where     X=byte of record that starts the field
> Y=length of the field, not counting any trailing  delimiter
>             
>             
> List of formats used for record below:
> Time of day: "hhmmss", where hh=hour "00" - "23" ; mm=minute "00" - "59";ss=seconds > "00" - "59". Always 6 digits.
> Spot length: Always in seconds, always 3 digits "000" - "999"
> 
> 
> Blank:HEX(20) (decimal 32) character is used for blanks.
> 
> Spot as aired record  One for each spot aired. Each record follows
> this layout:
> 
> 1,6  Aired time, using "Time of day" format above.  
> 7,1  HEX(7C) delimiter
> 8,2  Blank 
> 10,1 HEX(7C) delimiter
> 11,29 Name of spot or event (literal, padded with trailing blanks) 
> 40,1 HEX(7C) delimiter
> 41,12 Cart number/Media Number as aired (literal, left justified, padded with > trailing blanks)
> 53,1 HEX(7C) delimiter
> 54,12 BLANK    
> 66,1 HEX(7C) delimiter
> 67,3 Spot length as aired (spot length format), 
> 70,1 HEX(7C) delimiter
> 71,3 BLANK
> 74,1 HEX(7C) delimiter
> 75,8 BLANK
> 83,1 HEX(7C) delimiter
> 84,3 Announcement type [79].  (Literal, blank if unknown)
> 87,1 HEX(7C) delimiter
> 88,2 Automation system's error code to report.  Blank if no error, else zero padded > literal numeric.
> 90,1 HEX(7C) delimiter
> 91,16 Blank
> 107,1 HEX(7C) delimiter
> 108,4 Blank
> 112,1 HEX(7C) delimiter.  
> 
> Total record length: 113 + CR LF

A sample Wireready protocol file can be found in FFA:\\\\Ops\\Git Repositories\\The future of the marketron bot

NOTE: Roy warned me that if Wireready were to change their protocol, Marketron would be asked to also change their protocol. But he also said that it was unlikely that they would change it at this point. 