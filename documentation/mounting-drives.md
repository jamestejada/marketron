[Back to Main README](../README.md#mounting-network-drives)

# Mounting Network Drives in Ubuntu 20.04

Mounting a network drive is necessary to allow the program to access the .rtf paperless logs. 

## Mounting Network Drive on Windows Subsystem Linux (WSL) with Ubuntu 20.04
1. [Map a network drive](https://support.microsoft.com/en-us/windows/map-a-network-drive-in-windows-10-29ce55d1-34e3-a7e2-4801-131475f9557d) using the windows file system

1. Create the desired directory for the network mount in the `/mnt` directory
    ```
    $ sudo mkdir /mnt/paperless
    ```

1. Edit `/etc/fstab` and save
    - Example
    ```bash
    # /etc/fstab
    
    F:/continuity/promos /mnt/promos drvfs defaults 0 0
    P:/ /mnt/paperless drvfs defaults 0 0
    ```
1. Mount Shared Drives
    ```
    $ sudo mount -a
    ```


## Mounting Network Drive on Linux Machine (Ubuntu 20.04)
1. Install [cifs-utils](https://wiki.samba.org/index.php/LinuxCIFS_utils)
    ```
    $ sudo apt install cifs-utils
    ```

1. Create a credentials folder with access to the desired network drive
    - Example:
    ```bash
    # Network share credentials file
    # /root/creds

    username={user_name}
    password={password}
    domain={domain}
    ```

1. Edit `/mnt/fstab`
    - Example:
    ```bash
    # /etc/fstab

    //192.168.1.2/continuity/promos /mnt/promos cifs vers=2.0,credentials=/root/cred,iocharset=utf8 0 0
    //192.168.1.3/paperless /mnt/paperless cifs vers=2.0,credentials=/root/cred,iocharset=utf8 0 0 
    ```

1. Mount the Shared Drives
    ```
    $ sudo mount -a
    ```


[Back to Main README](../README.md#mounting-network-drives)
