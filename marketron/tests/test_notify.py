from marketron.notify import build_list_of_links
from marketron.tests.test_write import EXPECTED_WRITE


EXPECTED_NOTIFICATION_TEXT = ['• test_json_file -- dummy_link_from_json_file_test_json_file']

def test_build_list_of_links():
    test_output_write_dict = EXPECTED_WRITE.TEST_LINK_DICT
    actual_notify_text = build_list_of_links(output_write_dict=test_output_write_dict)
    assert actual_notify_text == EXPECTED_NOTIFICATION_TEXT
