from marketron.parse import Data_Extractor
from marketron.settings import TEST_TXT_PATH
import pytest

class EXPECTED_PARSE:
    FIRST_LINE = 'KKTO-FM     Program Log     Thursday 6/17/2021'
    STATION = 'KKTO-FM'
    DAY_OF_WEEK = 'Thursday'
    DATE_STRING = '6/17/2021'
    JOINED_SCRIPTS = [
        'Hour: 12 AM (Midnight)', 
        '12:00:00A - 01:59:00A\t** BBC WORLD SERVICE **', 
        '01:59:00A', 
        'From Sacramento State, this is Capital Public Radio, 90-point-5 KKTO Tahoe City/Reno and 88-1 KQNC Quincy, streaming at capradio-dot-org.', 
        '02:00:00A - 08:59:00A\t** MORNING EDITION (NPR-STR1) **',
        '05:49:25A',
        'This is CapRadio, 90-point-nine Sacramento and 90-point-five Tahoe-Reno, your NPR station. Streaming at capradio-dot-org, on your smart speaker, and on the CapRadio app.',
        '06:14:50A',
        'This is CapRadio, 88-point-nine Sacramento and 88-point-seven Sutter.',
        '06:59:10A - 06:59:40A\tUNDERWRITING PROMO\t5A / 7P', 
        'UW TESTIMONIAL--BOUTIN JONES USE DADPRO #00884', 
        '07:59:00A - 07:59:10A\tCONT ID + TRANS TO', 
        '07:59:00A', 
        'From Sacramento State, this is listener-supported Capital Public Radio: 90-point-5 KKTO Tahoe City-Reno, and 88-point-1 KQNC Quincy, with translators K215DS Truckee and K211EF South Lake Tahoe.', 
        '08:44:50A',
        'This is CapRadio, 88-point-nine Sacramento and 91-point-seven Groveland Sonora.',
        '08:59:25A - 08:59:40A\tCPR DEVELOPMENT\t6A / 7P',
        'CapRadio has been this region\'s definitive source of rigorous reporting and uplifting music since 1979. To continue creating a more informed and inspired public, plan to make a gift later with our legacy program. Learn more at capradio-dot-org-slash-legacy.',
        '03:15:15P',
        'FORWARD PROMOTE - national story from NPR day'
        ]
    OUTPUT_DICT = {
        'meta': {
            'station': 'KKTO-FM',
            'day_of_week': 'Thursday',
            'date_string': '6/17/2021'
        },
        '01:59:00A': {
            'air_window': None,
            'script': 'From Sacramento State, this is Capital Public Radio, 90-point-5 KKTO Tahoe City/Reno and 88-1 KQNC Quincy, streaming at capradio-dot-org.'
        },
        '06:59:10A-06:59:40A': {
            'air_window': '5A/7P',
            'script': 'UW TESTIMONIAL--BOUTIN JONES USE DADPRO #00884'
        },
        '07:59:00A': {
            'air_window': None,
            'script': 	'From Sacramento State, this is listener-supported Capital Public Radio: 90-point-5 KKTO Tahoe City-Reno, and 88-point-1 KQNC Quincy, with translators K215DS Truckee and K211EF South Lake Tahoe.'
        },
        '08:59:25A-08:59:40A': {
            'air_window': '6A/7P',
            'script': 'CPR DEVELOPMENT'
        }
    }


def test_Data_Extractor_join_scripts():
    parser = Data_Extractor(TEST_TXT_PATH)
    actual = parser.join_scripts(parser.formatted[1:])

    assert actual == EXPECTED_PARSE.JOINED_SCRIPTS


def test_Data_Extractor_get_metadata():
    parser = Data_Extractor(TEST_TXT_PATH)
    actual_get_metadata_method_results = parser.get_metadata(
        first_line=EXPECTED_PARSE.FIRST_LINE
        )
    instance_variable_metadata_results = parser.station, parser.day_of_week, parser.date_string
    expected = EXPECTED_PARSE.STATION, EXPECTED_PARSE.DAY_OF_WEEK, EXPECTED_PARSE.DATE_STRING

    assert actual_get_metadata_method_results == expected
    assert instance_variable_metadata_results == expected


def test_Data_Extractor_convert_to_dict():
    parser = Data_Extractor(TEST_TXT_PATH)
    joined_scripts = parser.join_scripts(parser.formatted[1:])
    actual_dict = parser.convert_to_dict(joined_scripts)

    assert actual_dict == EXPECTED_PARSE.OUTPUT_DICT
    
@pytest.mark.parametrize('windows_char', ['’', '“', '”','\x93', '\x85', '\x94'])
def test_Data_Extractor_replace_windows_characters(windows_char):
    parser = Data_Extractor(TEST_TXT_PATH)
    test_string_list = ['random ’', '“test”', 'string“', 'with', '”windows', 'characters']
    actual_list = parser.replace_windows_characters(test_string_list)
    for line in actual_list:
        assert windows_char not in line