from marketron.write import write_one, Spreadsheet_Builder, Drive_Operations
from marketron.settings import TEST_JSON_PATH
import pytest


SHEET_TITLE = 'test_sheet'


class EXPECTED_WRITE:
    VALUE_BODY = [
        ['test_json_file', None, None, None, None, None], 
        [None, 'TARGET TIME', 'SCRIPT', 'AIR WINDOW', 'SHIFT (initials)', 'NOTES (Was spot moved?)'], 
        [None, '01:59:00A', 'From Sacramento State, this is Capital Public Radio, 90-point-5 KKTO Tahoe City/Reno and 88-1 KQNC Quincy, streaming at capradio-dot-org.', None, None, None],
        [None, '06:59:10A-06:59:40A', 'UW TESTIMONIAL--BOUTIN JONES USE DADPRO #00884', '5A/7P', None, None], 
        [None, '07:59:00A', 'From Sacramento State, this is listener-supported Capital Public Radio: 90-point-5 KKTO Tahoe City-Reno, and 88-point-1 KQNC Quincy, with translators K215DS Truckee and K211EF South Lake Tahoe.', None, None, None], 
        [None, '08:59:25A-08:59:40A', 'CPR DEVELOPMENT', '6A/7P', None, None]
    ]
    RANGE_KEYS = ['sheetId', 'startRowIndex', 'endRowIndex', 'startColumnIndex', 'endColumnIndex']
    TEST_LINK_DICT = {'test_json_file': 'dummy_link_from_json_file_test_json_file'}


@pytest.fixture
def builder():
    return Spreadsheet_Builder(TEST_JSON_PATH, title=SHEET_TITLE)

@pytest.fixture
def drive_operator():
    return Drive_Operations()


def test_write_one():
    actual = write_one(TEST_JSON_PATH, no_api_call=True)
    expected = EXPECTED_WRITE.TEST_LINK_DICT

    assert actual
    assert actual == expected


def test_Spreadsheet_Builder_init_title_row(builder):
    assert len(builder.title_row) == len(builder.HEADER_ROW)


def test_Spreadsheet_Builder_init_title(builder):
    assert builder.title == SHEET_TITLE or builder.title == TEST_JSON_PATH.stem


def test_Spreadsheet_Builder_init_log_name(builder):
    assert builder.log_name == TEST_JSON_PATH.stem


def test_Spreadsheet_Builder_init_ids_link(builder):
    assert builder.spreadsheet_id is None
    assert builder.link is None
    assert builder.sheet_id is None


def test_Spreadsheet_Builder_build_value_body(builder):
    for builder_line, expected_line in zip(builder.build_value_body(), EXPECTED_WRITE.VALUE_BODY):
        print(builder_line)
        print(expected_line)
    assert builder.build_value_body() == EXPECTED_WRITE.VALUE_BODY


@pytest.mark.parametrize('strikethrough', [True, False])
@pytest.mark.parametrize('color', [True, False])
@pytest.mark.parametrize('column', range(7))
def test_Spreadsheet_Builder_one_formatting_rule(
    builder, strikethrough:bool, color:bool, column:int):

    expected_dict = {
        'addConditionalFormatRule': {
            'rule': {
                'ranges': [
                    {
                        'sheetId': None,
                        'startColumnIndex': column,
                        'startRowIndex': 1
                    }
                ], 
                'booleanRule': {
                    'condition': {
                        'type': 'CUSTOM_FORMULA',
                        'values': [
                                {
                                    'userEnteredValue': '=A2=true'
                                }
                            ]
                        },
                        'format': {}
                    }
                }
            }
        }

    format_dict = expected_dict.get('addConditionalFormatRule').get('rule').get('booleanRule').get('format')
    if strikethrough:
        format_dict.update({'textFormat': {'strikethrough': strikethrough}})
    if color:
        format_dict.update({'backgroundColor': builder.COMPLETE_COLOR})

    actual_dict = builder._one_formatting_rule(
        column=column, strikethrough=strikethrough, color=color
        )

    # assertions
    for each_range in actual_dict.get('addConditionalFormatRule').get('rule').get('ranges'):
        assert all([(key in EXPECTED_WRITE.RANGE_KEYS) for key in each_range.keys()])
    assert actual_dict == expected_dict


@pytest.mark.parametrize('start_column_index', range(2))
@pytest.mark.parametrize('end_column_index', range(2))
@pytest.mark.parametrize('start_row_index', range(2))
@pytest.mark.parametrize('end_row_index', range(2))
def test_Spreadsheet_Builder_merge_cells(
    builder, start_column_index:int, end_column_index:int,
    start_row_index:int, end_row_index:int):

    default_end_column = 5
    default_end_row = 1

    _end_column_index = end_column_index \
        if end_column_index > start_column_index else default_end_column
    _end_row_index = end_row_index \
        if end_row_index > start_row_index else default_end_row

    expected = {
            'mergeCells': {
                'mergeType': 'MERGE_ROWS',
                'range': {
                    'sheetId': None,
                    'startColumnIndex': start_column_index,
                    'endColumnIndex': _end_column_index,
                    'startRowIndex': start_row_index,
                    'endRowIndex': _end_row_index
                }
            }
        }
    actual = builder.merge_cells(
        start_column_index=start_column_index,
        end_column_index=end_column_index,
        start_row_index=start_row_index,
        end_row_index=end_row_index
        )
    assert actual == expected


@pytest.mark.parametrize('start_column_index', range(0, 6, 3))
@pytest.mark.parametrize('end_column_index', range(0, 6, 3))
@pytest.mark.parametrize('start_row_index', range(0, 6, 3))
@pytest.mark.parametrize('end_row_index', range(0, 6, 3))
def test_Spreadsheet_Builder_data_validation(
    builder, start_row_index:int, end_row_index:int,
    start_column_index:int, end_column_index:int
    ):

    default_end_column = 1
    default_end_row = builder.rows

    _end_column_index = end_column_index \
        if end_column_index > start_column_index else default_end_column
    _end_row_index = end_row_index \
        if end_row_index > start_row_index else default_end_row

    expected_dict = {
        'repeatCell': {
            'range': {
                'sheetId': None,
                'startRowIndex': start_row_index,
                'endRowIndex': _end_row_index,
                'startColumnIndex': start_column_index,
                'endColumnIndex': _end_column_index
            },
            'cell': {
                'dataValidation': {
                    'condition': {
                        'type': 'BOOLEAN'
                        }
                },
                'userEnteredFormat':{
                    'verticalAlignment': 'BOTTOM'
                }
            },
            'fields': 'dataValidation,userEnteredFormat.verticalAlignment'
        }
    }
    actual_dict = builder._data_validation(
        start_row_index=start_row_index,
        end_row_index=end_row_index,
        start_column_index=start_column_index,
        end_column_index=end_column_index
    )

    assert expected_dict == actual_dict

@pytest.mark.parametrize('rows_or_columns', ['COLUMNS', 'ROWS'])
@pytest.mark.parametrize('start_index', range(3))
@pytest.mark.parametrize('end_index', range(0,6,3))
def test_Spreadsheet_Builder_set_autoresize_dimension(
    builder, rows_or_columns:str, start_index:int, end_index:int
    ):
    
    default_end_index = start_index + 1
    _end_index = end_index if end_index > start_index else default_end_index

    expected_dict = {
        'autoResizeDimensions': {
            'dimensions': {
                'sheetId': None,
                'dimension': rows_or_columns,
                'startIndex': start_index,
                'endIndex': _end_index
            }
        }
    }

    actual_dict = builder._set_autoresize_dimension(
        rows_or_columns=rows_or_columns,
        start_index=start_index,
        end_index=end_index
    )

    assert expected_dict == actual_dict


@pytest.mark.parametrize('rows_or_columns', ['COLUMNS', 'ROWS'])
@pytest.mark.parametrize('start_index', range(3))
@pytest.mark.parametrize('end_index', range(0,6,3))
@pytest.mark.parametrize('size_in_pixels', range(100, 300, 50))
def test_Spreadsheet_Builder_set_fixed_dimension(
    builder, size_in_pixels:int, rows_or_columns:str,
    start_index:int, end_index:int):

    default_end_index = start_index + 1
    _end_index = end_index if end_index > start_index else default_end_index

    expected_dict = {
        'updateDimensionProperties': {
            'range': {
                'sheetId': None,
                'dimension': rows_or_columns,
                'startIndex': start_index,
                'endIndex': _end_index
            },
            'properties': {
                'pixelSize': size_in_pixels
            },
            'fields': 'pixelSize'
        }
    }

    actual_dict = builder._set_fixed_dimension(
        size_in_pixels=size_in_pixels,
        rows_or_columns=rows_or_columns,
        start_index=start_index,
        end_index=end_index
    )

    assert actual_dict == expected_dict


def test_Spreadsheet_Builder_format_title(builder):
    expected = {
            'repeatCell': {
                'range': {
                    'sheetId': None,
                    'startRowIndex': 0,
                    'endRowIndex': 1,
                    'endColumnIndex': 1
                },
                'cell': {
                    'userEnteredFormat': {
                        'textFormat': {
                            'bold': True,
                            'fontSize': 12
                        },
                        'horizontalAlignment': 'CENTER',
                        'verticalAlignment': 'MIDDLE',
                        'backgroundColor': builder.TITLE_BACKGROUND_COLOR
                    }
                },
                'fields': ','.join(
                    [
                        'userEnteredFormat.textFormat.bold',
                        'userEnteredFormat.textFormat.fontSize',
                        'userEnteredFormat.horizontalAlignment',
                        'userEnteredFormat.verticalAlignment',
                        'userEnteredFormat.backgroundColor'
                    ]
                )
            }
        }
    actual = builder._format_title()

    assert actual == expected

@pytest.mark.parametrize('start_column_index', range(3))
@pytest.mark.parametrize('end_column_index', range(3))
def test_Spreadsheet_Builder_set_text_wrap(builder, start_column_index:int, end_column_index:int):
    
    default_column_end_index = start_column_index + 1
    _end_column_index = end_column_index if end_column_index > start_column_index else default_column_end_index
    
    expected = {
            'repeatCell': {
                'range': {
                    'sheetId': None,
                    'startRowIndex': 0,
                    # 'endRowIndex': ommitted to extend to end of sheet
                    'startColumnIndex': start_column_index,
                    'endColumnIndex': _end_column_index
                },
                'cell': {
                    'userEnteredFormat': {
                        'wrapStrategy': 'WRAP'
                    }
                },
                'fields': 'userEnteredFormat.wrapStrategy'
            }
        }
    
    actual = builder._set_text_wrap(
        start_column_index=start_column_index,
        end_column_index=end_column_index
    )

    assert actual == expected

    expected.get('repeatCell').get('range').update(
            {
                'startColumnIndex': 2,
                'endColumnIndex': 3
            }
        )
    no_param_actual = builder._set_text_wrap()
    assert expected == no_param_actual


@pytest.fixture
def mock_super_super_folder():
    return ['super_folder_string']

@pytest.fixture
def mock_sheet_response_super_super(mock_super_super_folder):
    return {
        'parents': [mock_super_super_folder[-1]]
    }

@pytest.fixture
def mock_super_folders():
    return ['super_folder_string','random_string']

@pytest.fixture
def mock_sheet_response_super(mock_super_folders):
    return {
        'parents': [mock_super_folders[-1]]
    }

@pytest.fixture
def mock_sub_folders():
    return ['super_folder_string','random_string', 'random_string_for_sub_folder']

@pytest.fixture
def mock_sheet_response_sub(mock_sub_folders):
    return {
        'parents': [mock_sub_folders[-1]]
    }

def test_Drive_Operations_get_destination_folder_move_to_subfolder(
    drive_operator, mock_sheet_response_super, mock_sub_folders, mock_super_folders
    ):

    assert drive_operator._get_destination_folders (sheet_response=mock_sheet_response_super, destination_parent_ids=mock_sub_folders) == ([mock_super_folders[-1]], mock_sub_folders)

def test_Drive_Operations_get_destination_folder_move_to_superfolder(
    drive_operator, mock_sub_folders, mock_super_folders, mock_sheet_response_sub, mock_super_super_folder
    ):
    assert drive_operator._get_destination_folders(
        sheet_response=mock_sheet_response_sub, destination_parent_ids=mock_super_folders
        ) == ([mock_sub_folders[-1]], mock_super_folders)
    assert drive_operator._get_destination_folders(
        sheet_response=mock_sheet_response_sub, destination_parent_ids=mock_super_super_folder
        ) == ([mock_sub_folders[-1]], mock_super_super_folder)


def test_Drive_Operations_get_destination_do_not_move_to_super(drive_operator, mock_super_folders, mock_sheet_response_super):
    assert drive_operator._get_destination_folders(sheet_response=mock_sheet_response_super, destination_parent_ids=mock_super_folders) == (None, None)


@pytest.mark.parametrize('folders', [mock_sub_folders, mock_super_folders, mock_super_super_folder])
def test_Drive_Operations_get_destination_handle_empty_sheet_response(drive_operator, folders):
    assert drive_operator._get_destination_folders(sheet_response={}, destination_parent_ids=folders) == (None, None)


