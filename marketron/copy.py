import shutil
from marketron.settings import PAPERLESS_DIR, PAPERLESS_STATIONS, RTF_DIR
from colorama import Fore, Style
from datetime import datetime


def copy_one_log(source_path, destination_path=None):
    _destination_path = destination_path or RTF_DIR.joinpath(source_path.name)
    return shutil.copy(str(source_path), str(_destination_path))


def copy_one_archive_file(source_path, destination_path=None):
    return shutil.copy(str(source_path), str(destination_path))


def copy_all_logs():
    copy_list = []
    for station in PAPERLESS_STATIONS:
        station_path = PAPERLESS_DIR.joinpath(station)
        for rtf_file in station_path.iterdir():
            if check_rtf_file(rtf_file):
                print(f'Copying {rtf_file.name}...', flush=True, end='')
                copy_list.append(rtf_file)
                copy_one_log(rtf_file)
                print(Fore.GREEN, 'DONE', Style.RESET_ALL)
    return copy_list

def check_rtf_file(target_file):
    return (
        'rtf' in target_file.suffix
        and '~' not in target_file.stem
        and '$' not in target_file.stem
        and is_newer(target_file)
    )

def is_newer(target_file):
    local_file = RTF_DIR.joinpath(target_file.name)
    if not local_file.exists():
        return True
    local_file_time = datetime.fromtimestamp(local_file.stat().st_mtime)
    target_file_time = datetime.fromtimestamp(target_file.stat().st_mtime)
    return local_file_time < target_file_time
    