import pytest
import shutil

from marketron.copy import copy_all_logs
from marketron.convert import convert_one
from marketron.parse import parse_one
from marketron.write import write_one, empty_drive_trash, archive_pdf
from marketron.notify import slack_message
from marketron.settings import FLAGS, RTF_DIR, TXT_DIR, JSON_DIR


def main():
    if FLAGS.RESET:
        empty_drive_trash()
        reset()
        return

    if FLAGS.TESTS:
        args = '-vv' if FLAGS.VERBOSE else ''
        pytest.main(args=[args])
        return

    if FLAGS.COPY:
        copy_all_logs()
        return
    
    if FLAGS.ONE:
        empty_drive_trash()
        copy_all_logs()
        for rtf_file in RTF_DIR.iterdir():
            sheets_link = run_one(rtf_file.stem)
            print(sheets_link)
            # slack_message(sheets_link)
            return
        return
    
    if FLAGS.ARCHIVE:
        archive_pdf()
        return

    run_all()


def run_all():
    empty_drive_trash()
    process_list = copy_all_logs()
    link_dict = {}
    for each_file in process_list:
        link_dict.update(run_one(each_file.stem))
    if link_dict:
        slack_message(link_dict)


def run_one(log_name):
    convert_one(RTF_DIR.joinpath(f'{log_name}.rtf'))
    parse_one(TXT_DIR.joinpath(f'{log_name}.txt'))
    return write_one(JSON_DIR.joinpath(f'{log_name}.json'))


def reset():
    """Clears 'data' directory"""
    empty_drive_trash()
    for directory in [RTF_DIR, JSON_DIR, TXT_DIR]:
        shutil.rmtree(str(directory), ignore_errors=True)


if __name__ == '__main__':
    main()
