from colorama import Fore, Style
from marketron.settings import (
    CREDENTIALS_PATH, FLAGS, GOOGLE_DRIVE_FOLDERS, ARCHIVE_DIR, PAPERLESS_ARCHIVE
    )
from marketron.copy import copy_one_archive_file
import json
import time

from googleapiclient.discovery import build
from google.oauth2.service_account import Credentials
from googleapiclient.errors import HttpError


class Google_Auth:
    CREDENTIALS_PATH = CREDENTIALS_PATH
    SCOPES = [
        'https://www.googleapis.com/auth/spreadsheets',
        'https://www.googleapis.com/auth/drive'
        ]

    def __init__(self):
        self.creds = self.get_creds()

    def get_creds(self):
        return Credentials.from_service_account_file(self.CREDENTIALS_PATH, scopes=self.SCOPES)

    def create_service(self, api_name:str, version:str):
        return build(api_name, version, credentials=self.creds)


class Drive_Operations(Google_Auth):
    """Performs Google Drive Operations"""
    GOOGLE_DRIVE_FOLDERS = GOOGLE_DRIVE_FOLDERS
    ARCHIVE_DIR = ARCHIVE_DIR

    def __init__(self):
        super().__init__()
        self.drive_service = self.create_service('drive', 'v3')
    
    def move_file_to_drive_folder(self, spreadsheet_id, log_name, destination_folder_ids:list=None):
        """Moves current sheet into destination folder in Google drive"""

        sheet_file = self.drive_service.files().get(
                fileId=spreadsheet_id,
                fields='parents'
            ).execute()

        previous_parents, destination_parents = self._get_destination_folders(
                                sheet_response=sheet_file,
                                destination_parent_ids=destination_folder_ids,
                                log_name=log_name
                            )
        if not previous_parents and not destination_parents:
            return

        return self.drive_service.files().update(
            fileId=spreadsheet_id,
            addParents=','.join(destination_parents),
            removeParents=','.join(previous_parents),
            fields='id, parents'
        ).execute()

    def _get_destination_folders(
                self, sheet_response:dict,
                destination_parent_ids:list=None, log_name:str=None
                ):
        """returns previous_parents, destination_parents if the file should be moved,
        or else it returns None, None"""
        if not sheet_response:
            return None, None
        previous_parents = sheet_response.get('parents')
        destination_parents = self._get_new_parents(log_name, destination_parent_ids)
        if destination_parents[-1] == previous_parents[-1]:
            return None, None
        return previous_parents, destination_parents

    def _get_new_parents(self, log_name:str=None, new_parents:list=None):
        station = self._get_station(log_name)
        return new_parents or [
            # self.GOOGLE_DRIVE_FOLDERS.get('marketron'),
            self.GOOGLE_DRIVE_FOLDERS.get(station)
        ]

    def _get_station(self, log_name:str=None):
        if not log_name:
            return
        for station in self.GOOGLE_DRIVE_FOLDERS.keys():
            if station in log_name:
                return station

    def get_drive_file_list(self):
        drive_result = self.drive_service.files().list().execute()
        return drive_result.get('files')

    def empty_drive_trash(self):
        self.drive_service.files().emptyTrash().execute()
    
    def archive_files(self):
        """Archives files within the Marketron folder on Google Drive"""
        archive_files_list = self._get_files_list_by_folder()
        for archive_file in archive_files_list:
            file_id = archive_file.get('id')
            file_name = archive_file.get('name')
            file_path = self.ARCHIVE_DIR.joinpath(f'{file_name}.pdf')

            if not file_path.exists():
                pdf_data = self._get_pdf_data(file_id)
                self._write_archive_file(file_path, pdf_data)

    def _get_files_list_by_folder(self, folder_id:str=None):
        _folder_id = folder_id or self.GOOGLE_DRIVE_FOLDERS.get('ARCHIVE')
        archive_folder_response = self.drive_service.files().list(
            # NOTE: quotations are needed around _folder_id for request to work
            q=f'"{_folder_id}" in parents',
            fields='files(id,name)'
        ).execute()
        return archive_folder_response.get('files')
    
    def _get_pdf_data(self, file_id:str):
        return self.drive_service.files().export(
            fileId=file_id,
            mimeType='application/pdf'
        ).execute()

    def _write_archive_file(self, new_file_path, pdf_data):
        with open(new_file_path, 'wb+') as outfile:
            outfile.write(pdf_data)


class Spreadsheet_Builder(Google_Auth):

    COMPLETE_COLOR = {'red': 1.0, 'green': 0.85, 'blue': 0.73}
    TITLE_BACKGROUND_COLOR = {'green': 0.80, 'red': 0.80, 'blue': 0.82}
    PADDING = {'top': 5, 'bottom': 5}
    BODY_FONT_SIZE = 12
    TITLE_FONT_SIZE = 12
    HEADER_ROW_FONT_SIZE = 12
    HEADER_ROW = [
        None,
        'TARGET TIME',
        'SCRIPT',
        'AIR WINDOW',
        'SHIFT (initials)',
        'NOTES (Was spot moved?)'
    ]

    def __init__(self, input_json_file, title=None):
        super().__init__()
        self.scripts_json = self.read_json(input_json_file)
        self.rows = len(self.scripts_json) + 1
        self.columns = len(self.HEADER_ROW)
        self.log_name = input_json_file.stem
        self.title_row = [self.log_name, *[None for _ in range(self.columns - 1)]]
        self.title = title or self.log_name

        self.service = self.create_service('sheets', 'v4') # from super class
        self.drive_instance = Drive_Operations()

        self.spreadsheet_id = None  #  set when new spreadsheet is built
        self.link = None
        self.sheet_id = None    #  set when we get the specific sheet id
                                #  within spreadsheet

    # main
    def build_sheet(self):
        spreadsheet_info = self.find_or_create_sheet()
        self.spreadsheet_id = spreadsheet_info.get('spreadsheetId')
        self.link = f'https://docs.google.com/spreadsheets/d/{self.spreadsheet_id}'
        self.sheet_id = spreadsheet_info.get('sheets')[0].get('properties').get('sheetId')
        self.update_values()
        self.create_formatting()

        self.drive_instance.move_file_to_drive_folder(self.spreadsheet_id, self.log_name)

        return self.link

    def find_existing(self, log_name):
        """Attempts to find an existing google sheet whose title matches self.log_name. 
        If it is not found, this function returns None
        """
        drive_file_list = self.drive_instance.get_drive_file_list()
        for each_file in drive_file_list:
            if each_file.get('name').strip() == log_name:
                return self.service.spreadsheets().get(
                    spreadsheetId=each_file.get('id')
                ).execute()
        # return None if spreadsheet cannot be found

    def find_or_create_sheet(self) -> str:
        """Creates a new google sheet (or finds an existing sheet of the same log name)
        and returns the ID"""

        existing_sheet = self.find_existing(self.log_name)
        if existing_sheet:
            return existing_sheet

        properties = {
            'properties': {
                'title': self.title
            },
            'sheets': [
                {
                    'properties': {
                        'title': self.log_name
                    }
                }
            ]
        }
        return self.service.spreadsheets().create(
            body=properties,
            fields='spreadsheetId,sheets'
        ).execute()
    
    def create_formatting(self):
        formatting_body = {'requests': self.build_formatting_body()}
        return self.service.spreadsheets().batchUpdate(
            spreadsheetId=self.spreadsheet_id,
            body=formatting_body
        ).execute()

    def update_values(self, spreadsheet_id=None, data_list: list = None) -> dict:
        """Populates the new spreadsheet with values from the json scripts file
        and returns the json response"""
        data = data_list if data_list else self.build_value_body()
        value_body = {'values': data}
        return self.service.spreadsheets().values().update(
            spreadsheetId=spreadsheet_id or self.spreadsheet_id,
            range=self.log_name,
            valueInputOption='USER_ENTERED',
            body=value_body
        ).execute()

    def build_value_body(self):
        """creates list of lists for spreadsheet data body sent to 
        google sheets api"""
        output = [
            self.title_row,
            self.HEADER_ROW
        ]
        for time_string, info_dict in self.scripts_json.items():
            if time_string == 'meta':
                continue
            output.append(
                [
                    None,
                    time_string,
                    info_dict.get('script'),
                    info_dict.get('air_window'),
                    None,
                    None
                ]
            )
        return output

    def build_formatting_body(self):
        """creates a list of dicts that will format properties of cells"""
        return [
            self._set_bold(),
            self.set_text_font_all(),
            self._format_header(),
            self._format_title(),
            self._set_fixed_dimension(40, 'COLUMNS', start_index=0, end_index=1),
            self._set_autoresize_dimension('COLUMNS', start_index=1, end_index=2),
            self._set_fixed_dimension(400, 'COLUMNS', start_index=2, end_index=3),
            self._set_autoresize_dimension('COLUMNS', start_index=3, end_index=5),
            self._set_fixed_dimension(400, 'COLUMNS', start_index=5, end_index=6),
            self._set_autoresize_dimension('ROWS', start_index=0, end_index=self.rows),
            self._set_text_wrap(start_column_index=2, end_column_index=3),
            self._data_validation(),
            *self._set_conditional_formatting(),
            self.merge_cells(
                start_column_index=0, end_column_index=5,
                start_row_index=0, end_row_index=1
                ),
            self.freeze_header_rows(),
            self._add_padding_to_rows()
        ]

    def set_text_font_all(self, font_size=None, start_column_index=1, end_column_index=7):
        _end_column_index = self._end_index(
            start_index=start_column_index,
            end_index=end_column_index,
            default=start_column_index + 1
            )

        return {
            'repeatCell': {
                'range': {
                    'sheetId': self.sheet_id,
                    # start row begins after the title
                    'startRowIndex': 2,
                    # 'endRowIndex': ommitted to extend to end of sheet
                    'startColumnIndex': start_column_index,
                    'endColumnIndex': _end_column_index
                },
                'cell': {
                    'userEnteredFormat': {
                        'textFormat': {'fontSize': font_size or self.BODY_FONT_SIZE}
                    }
                },
                'fields': 'userEnteredFormat.textFormat.fontSize'
            }
        }

    def freeze_header_rows(self):
        return {
            'updateSheetProperties': {
                'properties': {
                    'sheetId': self.sheet_id,
                    'gridProperties': {
                        'frozenRowCount': 2
                    }
                },
                'fields': 'gridProperties.frozenRowCount'
            }
        }

    def _set_conditional_formatting(self):
        return [
            *[
                self._one_formatting_rule(column, strikethrough=False, color=True)
                for column in range(1,5)
            ],
            *[
                self._one_formatting_rule(column, strikethrough=False, color=True) 
                for column in range(5, self.columns)
            ],
            self._one_formatting_rule(0, strikethrough=False, color=True)
        ]

    def _conditional_format_field(self, strikethrough, color):
        output = {}
        if strikethrough:
            output.update({'textFormat': {'strikethrough': True}})
        if color:
            output.update({'backgroundColor': self.COMPLETE_COLOR})
        return output

    def _one_formatting_rule(self, column, strikethrough=False, color=True):
        return {
            'addConditionalFormatRule': {
                'rule': {
                    'ranges': [
                        {
                            'sheetId': self.sheet_id,
                            'startColumnIndex': column,
                            # 'endColumnIndex': 6,
                            'startRowIndex': 1
                            # # 'endRowIndex': 6
                        }
                    ],
                    'booleanRule': {
                        'condition': {
                            'type': 'CUSTOM_FORMULA',
                            'values': [
                                {
                                    'userEnteredValue': '=A2=true'
                                }
                            ]
                        },
                        'format': self._conditional_format_field(
                            strikethrough=strikethrough,
                            color=color
                        )
                    }
                }
                # 'index': 0
            }
        }

    def _set_text_wrap(self, start_column_index:int=2, end_column_index:int=3):

        _end_column_index = self._end_index(
            start_index=start_column_index,
            end_index=end_column_index,
            default=start_column_index + 1
            )

        return {
            'repeatCell': {
                'range': {
                    'sheetId': self.sheet_id,
                    'startRowIndex': 0,
                    # 'endRowIndex': ommitted to extend to end of sheet
                    'startColumnIndex': start_column_index,
                    'endColumnIndex': _end_column_index
                },
                'cell': {
                    'userEnteredFormat': {
                        'wrapStrategy': 'WRAP'
                    }
                },
                'fields': 'userEnteredFormat.wrapStrategy'
            }
        }

    # XXX: ADD TEST FOR THIS
    def _set_bold(self, start_column_index=5, end_column_index=None):

        _end_column_index = self._end_index(
            start_index=start_column_index,
            end_index=end_column_index,
            default=start_column_index + 1
            )

        return {
            'repeatCell': {
                'range': {
                    'sheetId': self.sheet_id,
                    'startRowIndex': 0,
                    # 'endRowIndex': ommitted to extend to end of sheet
                    'startColumnIndex': start_column_index,
                    'endColumnIndex': _end_column_index
                },
                'cell': {
                    'userEnteredFormat': {
                        'textFormat': {
                            'bold': True
                        }
                    }
                },
                'fields': 'userEnteredFormat.textFormat.bold'

            }
        }
    
    # XXX: ADD TEST FOR THIS
    def _add_padding_to_rows(self, start_column_index=2, end_column_index=None):
        _end_column_index = self._end_index(
                start_index=start_column_index,
                end_index=end_column_index,
                default=self.columns
            )
        return {
            'repeatCell': {
                'range': {
                    'sheetId': self.sheet_id,
                    'startRowIndex': 2,
                    # endRowIndex: ommitted to extend to end of sheet
                    'startColumnIndex': start_column_index,
                    'endColumnIndex': _end_column_index
                },
                'cell': {
                    'userEnteredFormat': {'padding': self.PADDING}
                },
                'fields': 'userEnteredFormat.padding.top,userEnteredFormat.padding.bottom'
            }
        }

    def _format_title(self, font_size=None):
        return {
            'repeatCell': {
                'range': {
                    'sheetId': self.sheet_id,
                    'startRowIndex': 0,
                    'endRowIndex': 1,
                    'endColumnIndex': 1
                },
                'cell': {
                    'userEnteredFormat': {
                        'textFormat': {
                            'bold': True,
                            'fontSize': font_size or self.TITLE_FONT_SIZE
                        },
                        'horizontalAlignment': 'CENTER',
                        'verticalAlignment': 'MIDDLE',
                        'backgroundColor': self.TITLE_BACKGROUND_COLOR
                    }
                },
                'fields': ','.join(
                    [
                        'userEnteredFormat.textFormat.bold',
                        'userEnteredFormat.textFormat.fontSize',
                        'userEnteredFormat.horizontalAlignment',
                        'userEnteredFormat.verticalAlignment',
                        'userEnteredFormat.backgroundColor'
                    ]
                )
            }
        }

    def _format_header(
        self, font_size=None, bold=True, border:str='bottom', border_style:str='SOLID_THICK'):

        return {
            'repeatCell': {
                'range': {
                    'sheetId': self.sheet_id,
                    'startRowIndex': 1,
                    'endRowIndex': 2,
                    'endColumnIndex': self.columns
                },
                'cell': {
                    'userEnteredFormat': {
                        'textFormat': {
                            'bold': bold,
                            'fontSize': font_size or self.HEADER_ROW_FONT_SIZE
                        },
                        'borders': {
                            border: {
                                'style': border_style
                            }
                        }
                    }
                },
                'fields': ','.join(
                        [
                            'userEnteredFormat.textFormat.bold',
                            'userEnteredFormat.textFormat.fontSize',
                            'userEnteredFormat.borders.bottom.style'
                        ]
                    )
            }
        }

    def _set_fixed_dimension(
                self, size_in_pixels: int,  rows_or_columns: str = 'COLUMNS',
                start_index: int = 0, end_index: int = None) -> dict:
        """sets a column or row of a certain range to a fixed height/width"""

        _end_index = self._end_index(
            start_index=start_index,
            end_index=end_index,
            default=start_index + 1
            )

        return {
            'updateDimensionProperties': {
                'range': {
                    'sheetId': self.sheet_id,
                    'dimension': rows_or_columns,
                    'startIndex': start_index,
                    'endIndex': _end_index
                },
                'properties': {
                    'pixelSize': size_in_pixels
                },
                'fields': 'pixelSize'
            }
        }
    
    def _set_autoresize_dimension(
                self, rows_or_columns: str = 'COLUMNS', start_index: int = 0,
                end_index: int = None) -> dict:

        _end_index = self._end_index(
            start_index=start_index,
            end_index=end_index,
            default=start_index + 1
            )
        
        return {
            'autoResizeDimensions': {
                'dimensions': {
                    'sheetId': self.sheet_id,
                    'dimension': rows_or_columns,
                    'startIndex': start_index,
                    'endIndex': _end_index
                }
            }
        }

    def _data_validation(
        self, start_row_index:int=2, end_row_index=None,
        start_column_index:int=0, end_column_index:int=1):

        _end_column_index = self._end_index(start_column_index, end_column_index, default=1)
        _end_row_index = self._end_index(start_row_index, end_row_index, default=self.rows)

        return {
            'repeatCell': {
                'range': {
                    'sheetId': self.sheet_id,
                    'startRowIndex': start_row_index,
                    'endRowIndex': _end_row_index,
                    'startColumnIndex': start_column_index,
                    'endColumnIndex': _end_column_index
                },
                'cell': {
                    'dataValidation': {
                        'condition': {
                            'type': 'BOOLEAN'
                            }
                    },
                    'userEnteredFormat':{
                        'verticalAlignment': 'BOTTOM'
                    }
                },
                'fields': 'dataValidation,userEnteredFormat.verticalAlignment'
            }
        }

    def _end_index(self, start_index:int, end_index:int, default:int=None):
        # return default if not end_index or (start_index <= end_index) else end_index
        return end_index if end_index and start_index < end_index else default
    
    def merge_cells(
        self, start_column_index:int=0, end_column_index:int=None,
        start_row_index:int=0, end_row_index:int=None) -> dict:

        _end_column_index = self._end_index(start_column_index, end_column_index, default=5)
        _end_row_index = self._end_index(start_row_index, end_row_index, default=1)

        return {
            'mergeCells': {
                'mergeType': 'MERGE_ROWS',
                'range': {
                    'sheetId': self.sheet_id,
                    'startColumnIndex': start_column_index,
                    'endColumnIndex':  _end_column_index,
                    'startRowIndex': start_row_index,
                    'endRowIndex': _end_row_index
                }
            }
        }

    def read_json(self, input_json_file):
        with open(input_json_file, 'r') as infile:
            return json.loads(infile.read())

    def __str__(self) -> str:
        if self.spreadsheet_id and self.link:
            return f'LINK: {self.link}'
        return 'Sheet has not been built yet. Use the "<Spreadsheet_Builder>.build_sheet()"'


class Dummy_Builder:
    def __init__(self, json_file) -> None:
        self.json_file = json_file

    def build_sheet(self):
        self.link = f'dummy_link_from_json_file_{self.json_file.stem}'
        return self.link


# Private Functions
def _move_archive_to_paperless():
    for archive_file in ARCHIVE_DIR.iterdir():
        destination_file = PAPERLESS_ARCHIVE.joinpath(archive_file.name)
        if not destination_file.exists():
            copy_one_archive_file(archive_file, destination_file)


def _cooldown_countdown():
    cooldown = 20
    print(Fore.RED, Style.BRIGHT, 'RATE LIMIT HIT', Style.RESET_ALL)
    while cooldown >= 0:
        print(f'.....cooling down for {cooldown} seconds   ', end='\r', flush=True)
        time.sleep(1)
        cooldown -= 1
    print('\nRETRYING')


# External Functions
def write_one(json_file, no_api_call=FLAGS.NO_API_CALL):
    link = None
    while not link:
        try:
            print(f'Writing {json_file.stem} to Google Sheets...', flush=True, end='')
            builder = Dummy_Builder(json_file) if no_api_call else Spreadsheet_Builder(json_file)
            link = builder.build_sheet()

        except HttpError as e:
            message = json.laods(e.content).get('error', {'message': None}).get('message')
            if message:
                print(Fore.RED, message, Style.RESET_ALL)
            _cooldown_countdown()

    print(Fore.GREEN, 'DONE', Style.RESET_ALL)
    return {json_file.stem: link}


def empty_drive_trash():
    """Empties trash of Google Drive Account"""
    Drive_Operations().empty_drive_trash()


def archive_pdf():
    """Archives files found in Marketron Archive folder"""
    Drive_Operations().archive_files()
    _move_archive_to_paperless()
