import requests
from colorama import Fore, Style
from marketron.settings import SLACK_WEBHOOK


def slack_webhook(output_write_dict:dict):
    text = '\n'.join(build_list_of_links(output_write_dict))
    return requests.post(SLACK_WEBHOOK, json={'list_of_links': text})


def build_list_of_links(output_write_dict:dict):
    """returns a list of line strings with marketron
    log name and links to google sheet"""
    return [
        f'• {log_name} -- {link}'
        for log_name, link in output_write_dict.items()
        ]

def slack_message(link_dict:dict):
    """Sends the list of links to Slack workflow to notify users about new 
    spreadsheets"""
    response = slack_webhook(output_write_dict=link_dict)
    if response.ok:
        print(Fore.GREEN, 'Slack Message Sent\n\n', Style.RESET_ALL)
