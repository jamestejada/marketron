from pathlib import Path
import sys
import os
from dotenv import load_dotenv

def from_cwd(*path_list):
    directory = Path.cwd().joinpath(*path_list)
    directory.mkdir(exist_ok=True, parents=True)
    return directory


def check_flags(*flags) -> bool:
    return any([(arg.lower() in flags) for arg in sys.argv[1:]])

# booleans
class FLAGS:
    RESET = check_flags('reset', 'clear', 'clean')
    COPY = check_flags('copy', 'get')
    ONE = check_flags('one')
    TESTS = check_flags('tests', 'test')
    VERBOSE = check_flags('verbose', 'v')
    NO_API_CALL = check_flags('dry', 'dryrun', 'noapi')
    ARCHIVE = check_flags('archive')

RTF_DIR = from_cwd('marketron', 'data', 'rtf')
TXT_DIR = from_cwd('marketron', 'data', 'txt')
JSON_DIR = from_cwd('marketron', 'data', 'json')
CRED_DIR = from_cwd('marketron', 'credentials')
ARCHIVE_DIR = from_cwd('marketron', 'data', 'archive')

CREDENTIALS_PATH = CRED_DIR.joinpath('credentials.json')
ENV_FILE = CRED_DIR.joinpath('.env')
load_dotenv(ENV_FILE)

SLACK_WEBHOOK = os.getenv('SLACK_WEBHOOK')
GOOGLE_DRIVE_FOLDERS = {
    'marketron': os.getenv('MARKETRON_FOLDER_ID'),
    'KKTO': os.getenv('KKTO_FOLDER_ID'),
    'KUOP': os.getenv('KUOP_FOLDER_ID'),
    'KXJZ': os.getenv('KXJZ_FOLDER_ID'),
    'KXPR': os.getenv('KXPR_FOLDER_ID'),
    'ARCHIVE': os.getenv('ARCHIVE_FOLDER_ID')

}

PAPERLESS_DIR = Path('/mnt/paperless')
PAPERLESS_ARCHIVE = PAPERLESS_DIR.joinpath('Log Archive')
PAPERLESS_STATIONS = ['KKTO', 'KUOP', 'KXJZ', 'KXPR']

TEST_TXT_PATH = Path.cwd().joinpath(
    'marketron', 'tests', 'test_documents', 'test_txt_file.txt'
    )
TEST_JSON_PATH = Path.cwd().joinpath(
    'marketron', 'tests', 'test_documents', 'test_json_file.json'
    )
