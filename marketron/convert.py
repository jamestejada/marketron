from colorama import Fore, Style
from striprtf.striprtf import rtf_to_text
from marketron.settings import RTF_DIR, TXT_DIR


def convert_rtf_to_txt(input_file_path, output_file_path):

    with open(input_file_path, 'r', encoding='cp1252') as infile:
        infile_contents = infile.read()

    text_content = rtf_to_text(infile_contents)

    with open(output_file_path, 'w+') as outfile:
        outfile.write(text_content)


def convert_one(rtf_file):
    txt_file = TXT_DIR.joinpath(f'{rtf_file.stem}.txt')
    print(f'Converting {rtf_file.name} to txt...', flush=True, end='')
    convert_rtf_to_txt(rtf_file, txt_file)
    print(Fore.GREEN, 'DONE', Style.RESET_ALL)


# main
def convert_all():
    for rtf_file in RTF_DIR.iterdir():
        convert_one(rtf_file)
