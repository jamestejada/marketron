from colorama import Fore, Style
from marketron.settings import JSON_DIR
import json
import re

class Data_Extractor:

    TIME_RANGE_END = 3
    IGNORE_UNDERWRITER_LIST = ['UNDERWRITING PROMO']
    IGNORE_SCRIPTS = [
        'This is CapRadio, 88-point-nine Sacramento and 91-point-seven Groveland Sonora.',
        'This is CapRadio, 88-point-nine Sacramento and 88-point-seven Sutter.',
        'This is CapRadio, 90-point-nine Sacramento and 90-point-five Tahoe-Reno, your NPR station. Streaming at capradio-dot-org, on your smart speaker, and on the CapRadio app.',
        'This is CapRadio, 90-point-nine Sacramento and 91-point-three Stockton-Modesto, your NPR station. Streaming at capradio-dot-org, on your smart speaker, and on the CapRadio app.',
        'This is CapRadio, 90-point-nine Sacramento, your NPR station. Streaming at capradio-dot-org, on your smart speaker, and on the CapRadio app.',
        'FORWARD PROMOTE'
    ]

    def __init__(self, txt_input_file):
        self.input_file = txt_input_file
        self.txt_contents = self.read_file(self.input_file)
        self.raw_lines = self.split_lines(self.txt_contents)
        self.formatted = self.replace_windows_characters(self.raw_lines)
        first_line = self.raw_lines[0]
        self.station, self.day_of_week, self.date_string = self.get_metadata(first_line)

    # main
    def parse(self):
        joined = self.join_scripts(self.formatted[1:])
        output_dict = self.convert_to_dict(joined)
        self.write_file(output_dict, JSON_DIR.joinpath(f'{self.input_file.stem}.json'))

    def split_lines(self, corpus):
        return [
            line.strip() for line in corpus.split('\n')
            if line != '' and line != '\t'
        ]
    
    def replace_windows_characters(self, corpus_list: list) -> list:
        """Replaces windows characters that are not easily encodable to UTF-8"""
        output_list = corpus_list
        for replace_char in ['\x93', '\x85', '’', '“', '”']:
            output_list = [line.replace(replace_char, "'") for line in output_list]
        # replace with empty character instead of '
        output_list = [line.replace('\x94', '') for line in output_list]
        return output_list

    def get_metadata(self, first_line):
        station, _, _, dow, date_str = first_line.split()
        return station, dow, date_str
    
    def convert_to_dict(self, corpus_list: list):
        output_dict = {
                'meta': {
                    'station': self.station,
                    'day_of_week': self.day_of_week,
                    'date_string': self.date_string
                }
            }
        air_window_str, time_range, underwriter = None, None, None

        for line in corpus_list:
            if 'Hour: ' in line:
                air_window_str, underwriter = None, None
                continue
            if re.match(r'\d\d:\d\d:\d\d[A|P]', line):
                if re.match(r'\d\d:\d\d:\d\d[A|P] - \d\d:\d\d:\d\d[A|P]', line):
                    time_range, underwriter, air_window_str = self.parse_info_line(line)
                else:
                    time_range, underwriter, line = self.parse_other_date_line(line)
            else:
                if [script for script in self.IGNORE_SCRIPTS if script in line]:
                    air_window_str, time_range, underwriter = None, None, None
                    continue
                if underwriter in self.IGNORE_UNDERWRITER_LIST:
                    underwriter = None
                output_line = underwriter or line

                output_dict.update(
                    {
                        time_range: {
                            'air_window': air_window_str,
                            'script': output_line
                            # 'underwriter': underwriter
                        }
                    }
                )
        return  output_dict

    def parse_other_date_line(self, line: str):
        line_list = line.split()
        time_range = line_list[0]
        underwriter_as_replacement_line = ' '.join(line_list[1:])
        return time_range, None, underwriter_as_replacement_line

    def parse_info_line(self, line: str):
        line_list = line.split()
        time_range = ''.join(line_list[:self.TIME_RANGE_END])
        underwriter = self._get_underwriter_info_line(line_list)
        air_window = self._get_air_window_info_line(line_list)
        return time_range, underwriter, air_window
    
    def _get_air_window_info_line(self, line_list: list):
        if len(line_list) < self.TIME_RANGE_END:
            return None
        if '/' in line_list:
            air_window_index = line_list.index('/')
            window_start = air_window_index - 1
            window_end = air_window_index + 2
            return ''.join(line_list[window_start:window_end])

    def _get_underwriter_info_line(self, line_list: list):
        if '/' in line_list:
            underwriter_end = line_list.index('/') - 1
            return ' '.join(line_list[self.TIME_RANGE_END:underwriter_end])
        return ' '.join(line_list[self.TIME_RANGE_END:])

    def join_scripts(self, corpus_list: list) -> list:
        line_joiner = []
        output_list = []

        for line in corpus_list:
            if self._is_not_script(line):
                if line_joiner:
                    output_list.append(' '.join(line_joiner))
                line_joiner = []
                output_list.append(line)
            else:
                line_joiner.append(line)

        # catches last line of file
        if line_joiner:
            output_list.append(' '.join(line_joiner))

        return output_list

    def _is_not_script(self, line):
        return re.match(r'\d\d:\d\d:\d\d[A|P]', line) or 'Hour: ' in line

    def read_file(self, txt_input_file):
        with open(txt_input_file, 'r', encoding='utf8') as infile:
            return infile.read()

    def write_file(self, output_dict, json_output_file):
        with open(json_output_file, 'w+') as outfile:
            json.dump(output_dict, outfile)

# main
def parse_one(txt_file):
    if 'txt' in txt_file.suffix:
        print(f'Parsing {txt_file.name}...', flush=True, end='')
        parser = Data_Extractor(txt_file)
        parser.parse()
        print(Fore.GREEN, 'DONE', Style.RESET_ALL)
